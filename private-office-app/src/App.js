import React from 'react';
import styled from 'styled-components';
import Registration from './app/view/pages/Entry/Registration/RegistrationContainer';
import Login from './app/view/pages/Entry/Login/LoginContainer';
import Recovery from './app/view/pages/Entry/Recovery/RecoveryContainer';
import NewPass from './app/view/pages/Entry/NewPass/NewPassContainer';
import Welcome from './app/view/pages/Entry/Welcome/WelcomeContainer';
import Error from './app/view/pages/Entry/Error/Error';
import UserOffice from './app/view/pages/Private_office/UserOffice';
import { Route, Switch } from 'react-router-dom';

const AppComponent = styled.div``;

const PrOffice = () => {
	return (
		<Switch>
			<Route exact path={'/private_office'} component={Error} />
			<Route path={'/private_office/:name'} component={UserOffice} />
		</Switch>
	);
};

const App = () => {
	return (
		<AppComponent>
			<Switch>
				<Route exact path={'/'} component={Login} />
				<Route path={'/registration'} component={Registration} />
				<Route path={'/recovery'} component={Recovery} />
				<Route path={'/new_password'} component={NewPass} />
				<Route path={'/welcome'} component={Welcome} />
				<Route path={'/private_office'} component={PrOffice} />
				<Route path={'*'} component={Error} />
			</Switch>
		</AppComponent>
	);
};

export default App;

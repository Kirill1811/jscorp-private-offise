const initialState = {
	titleComponent: 'Новый пароль',

	fields: [
		{
			name: 'new_password',
			label: 'Ввведите новый пароль',
			type: 'password',
			normalize: [],
		},

		{
			name: 'repeat_password',
			label: 'Повторите пароль',
			type: 'password',
			normalize: [],
		},
	],

	btnComponent: 'Изменить пароль',

	infoComponents: {
		linkTo: '/*',
		linkText: 'Не пришло письмо?',
	},

	formName: 'new_pass',
};

export function new_pass(state = initialState) {
	return state;
}

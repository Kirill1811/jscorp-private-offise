const initialState = {
	titleComponent: 'Вход',
	fields: [
		{
			name: 'email',
			label: 'Email',
			type: 'email',
			normalize: [],
		},
		{
			name: 'password',
			label: 'Пароль',
			type: 'password',
			normalize: [],
		},
	],
	btnComponent: 'Войти',
	infoComponents: [
		{
			linkTo: '/recovery',
			linkText: 'Забыли пароль?',
		},
		{
			linkTo: '/registration',
			linkText: ' Регистрация',
		},
	],
	formName: 'login',
};

export function login(state = initialState) {
	return state;
}

import normalizePhone from '../utils/control/normalizePhone';
import normalizeFields from '../utils/control/normalizeFields';

const initialState = {
	titleComponent: 'Регистрация',

	fields: [
		{
			name: 'surname',
			label: 'Фамилия',
			type: 'text',
			normalize: [normalizeFields],
		},
		{
			name: 'name',
			label: 'Имя',
			type: 'text',
			normalize: [normalizeFields],
		},
		{
			name: 'patronymic',
			label: 'Отчество',
			type: 'text',
			normalize: [normalizeFields],
		},
		{
			name: 'city',
			label: 'Город',
			type: 'text',
			normalize: [normalizeFields],
		},
		{
			name: 'phone',
			label: 'Телефон',
			type: 'tel',
			normalize: [normalizePhone],
		},
		{
			name: 'email',
			label: 'Email',
			type: 'email',
			normalize: [],
		},
		{
			name: 'password',
			label: 'Пароль',
			type: 'password',
			normalize: [],
		},
	],

	checkboxesTitle: 'Кто вы?',

	checkboxes: [
		{
			name: 'role',
			type: 'radio',
			value: 'innovator',
			id: 'Choice1',
			label: 'Новатор',
		},
		{
			name: 'role',
			type: 'radio',
			value: 'specialist',
			id: 'Choice2',
			label: 'Специалист',
		},
		{
			name: 'role',
			type: 'radio',
			value: 'mentor',
			id: 'Choice3',
			label: 'Ментор',
		},
		{
			name: 'role',
			type: 'radio',
			value: 'projManager',
			id: 'Choice4',
			label: 'Менеджер проекта',
		},
	],

	mainCheckbox: {
		name: 'agreement',
		type: 'checkbox',
		id: 'PrivacyPolicy',

		label: ['Принимаю условия ', 'политики конфиденциальности'],
	},

	btnComponent: 'Войти',

	formName: 'registration',
};

export function registration(state = initialState) {
	return state;
}

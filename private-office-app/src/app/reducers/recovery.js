const initialState = {
	titleComponent: 'Восстановление пароля',

	fields: [
		{
			name: 'email',
			label: 'Email',
			type: 'email',
			normalize: [],
		},
	],

	btnComponent: 'Отправить письмо',

	infoComponents: {
		linkTo: '/*',
		linkText: 'Не пришло письмо?',
	},

	formName: 'recovery',
};

export function recovery(state = initialState) {
	return state;
}

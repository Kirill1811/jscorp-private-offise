const initialState = {
	titleComponent: 'Добро пожаловать в JS corp!',

	pComponents: [
		'Вы успешно зарегистрировались.',
		'Мы отправили вам email, пожалуйста перейдите в почту и верифицируйтесь!',
	],

	btns: ['Войти в личный кабинет', 'Вернуться на главную'],
};

export function welcomeForm(state = initialState) {
	return state;
}

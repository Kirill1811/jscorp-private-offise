import { combineReducers } from 'redux';
import { connectRouter } from 'connected-react-router';
import { reducer as formReducer } from 'redux-form';

import { login } from './login';
import { new_pass } from './new_pass';
import { recovery } from './recovery';
import { welcomeForm } from './welcomeForm';
import { registration } from './registration';
import { errorForm } from './error';
import { userOffice } from './userOffice';

export default history =>
	combineReducers({
		router: connectRouter(history),
		form: formReducer,
		login: login,
		new_pass: new_pass,
		recovery: recovery,
		welcomeForm: welcomeForm,
		registration: registration,
		errorForm: errorForm,
		userOffice: userOffice,
	});

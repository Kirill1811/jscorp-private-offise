export const SHOW_MENU = 'SHOW_MENU';
export const EDIT_PROFILE = 'EDIT_PROFILE';
export const SHOW_USER_MENU = 'SHOW_USER_MENU';

const initialState = {
	changMarker: false,

	showUserMenu: false,

	profileSendOut: false,

	menuPoints: [
		['Мой профиль', 'Мои Проекты', 'Мои документы'],

		'Материалы',

		['Исходный код', 'Объекты дизайна'],

		'Управление',

		[
			'Специалисты',
			'Новаторы',
			'Проекты',
			'Кураторы',
			'Менторы',
			'Стажировка',
			'Заявки',
		],

		'JS corporation',

		['Поддержка', 'Контакты', 'Web-site'],
	],

	userMenuPoints: ['Настройки', 'Поддержка', 'Выход'],

	profileTemplate: {
		btn_text: ['Редактировать профиль', 'Сохранить'],

		export: 'Экспорт',

		user: {
			personal_data: [
				' ',
				'Роль: ',
				'Дата рождения: ',
				'Телефон: ',
				'Email: ',
				'VK: ',
			],

			info: [
				'Информация',
				'Страна: ',
				'Город: ',
				'Пол: ',
				'Специальность: ',
				'Могу работать: ',
			],

			skills: [
				'Профильные навыки',
				'Web-дизайн',
				'Менеджмент',
				'Стартапы',
				'HTML',
				'CSS',
				'JavaScript',
			],

			about: '',
		},
	},
};

export function userOffice(state = initialState, action) {
	switch (action.type) {
		case SHOW_MENU:
			return { ...state, changMarker: action.payload };

		case EDIT_PROFILE:
			return { ...state, profileSendOut: action.payload };

		case SHOW_USER_MENU:
			return { ...state, showUserMenu: action.payload };

		default:
			return state;
	}
}

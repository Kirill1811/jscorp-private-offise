import { SubmissionError } from 'redux-form';

const sleep = ms => new Promise(resolve => setTimeout(resolve, ms));

function regSubmit(values) {
	return sleep(1000).then(() => {
		// Делаем запрос к базе
		// Проверяем наличие адреса в базе.
		// Если адрес уже есть, сообщаем об ошибке и о том, что регистрация не прошла
		if (values.email === 'vilyam13@gmail.com') {
			throw new SubmissionError({
				email: 'Этот адрес уже используется!',
				_error: 'Registration failed!',
			});
		} else {
			// Заносимданные нового пользователя в систему
			// Перенаправляем пользователя на страницу Welcome
			window.location.href = 'welcome';
		}
	});
}

export default regSubmit;

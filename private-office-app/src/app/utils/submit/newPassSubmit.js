import { SubmissionError } from 'redux-form';

const sleep = ms => new Promise(resolve => setTimeout(resolve, ms));

function newPassSubmit(values) {
	return sleep(1000).then(() => {
		if (values.new_password !== values.repeat_password) {
			// Проверяем совпадают ли пароли, введенные в двух окнах
			throw new SubmissionError({
				repeat_password: 'Не совпадает с введенным ранее',
				_error: 'Failed!',
			});
		} else {
			// Заносим новый пароль в базу
			// Отправляем пользователя на форму входа
			window.location.href = '/';
		}
	});
}

export default newPassSubmit;

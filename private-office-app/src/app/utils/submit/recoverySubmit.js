import { SubmissionError } from 'redux-form';

const sleep = ms => new Promise(resolve => setTimeout(resolve, ms));

function recoverySubmit(values) {
	return sleep(1000).then(() => {
		if (values.email !== 'vilyam13@gmail.com') {
			// Проверяем, есть ли такой адрес в базе
			throw new SubmissionError({
				email: 'Адрес отсутствует в базе!',
				_error: 'Failed!',
			});
		} else {
			// Отправляем запрос на изменение пароля

			window.location.href = '/new_password';
		}
	});
}

export default recoverySubmit;

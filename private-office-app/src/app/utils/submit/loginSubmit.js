import { SubmissionError } from 'redux-form';

const sleep = ms => new Promise(resolve => setTimeout(resolve, ms));

function loginSubmit(values) {
	return sleep(1000).then(() => {
		if (values.email !== 'vilyam13@gmail.com') {
			// Проверяем, есть ли такой адрес в базе
			throw new SubmissionError({
				email: 'Адрес отсутствует в базе!',
				_error: 'Failed!',
			});
		} else {
			// Проверяем, соответствует ли пароль адресу
			if (values.password !== 'dBkmzv13') {
				throw new SubmissionError({
					password: 'Пароль  указан неверно!',
					_error: 'Failed!',
				});
			} else {
				//
				const userName = values.email
					.split('@')
					.slice(0, 1)
					.join('');
				//window.ee.emit('User_login', values.email);
				window.location.href = '/private_office/' + userName;
			}
		}
	});
}

export default loginSubmit;

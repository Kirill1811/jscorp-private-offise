import {
	SHOW_MENU,
	EDIT_PROFILE,
	SHOW_USER_MENU,
} from '../reducers/userOffice';

export function showMenu(show) {
	return {
		type: SHOW_MENU,
		payload: show,
	};
}

export function editProfile(profileSendOut) {
	return {
		type: EDIT_PROFILE,
		payload: profileSendOut,
	};
}

export function showUserMenu(show) {
	return {
		type: SHOW_USER_MENU,
		payload: show,
	};
}

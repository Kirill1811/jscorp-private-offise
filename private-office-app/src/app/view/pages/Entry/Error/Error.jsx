import React from 'react';
import { connect } from 'react-redux';
import styled from 'styled-components';

const BoxWrapComponent = styled.div`
  margin: 186px auto;
  width: 100%;
  max-width: 396px;
  
  @media (max-width: 1199.98px) {
    max-width: 280px;
    margin: 150px auto;
  }
  
  @media (max-width: 575.98px) {
    max-width: 90%;
    margin: 10px auto;
  }
`;

const ErrorComponent = styled.div`
  margin: 286px auto 45px;
  width: 100%;
  max-width: 100%;
  -webkit-box-shadow: 1px 1px 8px #ccc;
          box-shadow: 1px 1px 8px #ccc;
  padding: 18px 40px;
  border-radius: 10px;
  position: relative;
  
  ::before {
      content: "";
      background-image: url(${require('../../../../img/entry/sorry.png')});
      background-repeat: no-repeat;
      background-size: 100%;
      display: block;
      width: 100%;
      max-width: 175px;
      min-height: 165px;
      position: absolute;
      top: -110px;
      left: 50%;
      -webkit-transform: translate(-50%, -50%);
          -ms-transform: translate(-50%, -50%);
              transform: translate(-50%, -50%);
      overflow: hidden;
  }
`;

const ErrorTitleComponent = styled.h3`
  margin: 0;
  color: #0eb8cc;
  text-align: center;
  font-family: "GhothemProBlack", sans-serif;
`;

const ErrorPComponent = styled.p`
  text-align: center;
  font-size: 14px;
  latter-spacing: 1.4;
`;

const BtnComponent = styled.div`
  color: #fff;
  margin-bottom: 16px;
  background: #0eb8cc;
  padding: 10px 15px;
  width: 100%;
  display: block;
  text-align: center;
  border-radius: 10px;
  cursor: pointer;
  
  :hover {
     background: #009aae;
     color: #fff;
     text-decoration: none;
  }
`;

const JSLogoComponent = styled.img`
  display: block;
  width: 50%;
  max-width: 250px;
  margin: 25px auto;
`;

const ErrorContainer = (props) => {
    const {
        titleComponent,
        pComponent,
        btn
    } = props.errorForm;


    return (
        <BoxWrapComponent>
            <ErrorComponent>
                <ErrorTitleComponent>
                    {titleComponent}
                </ErrorTitleComponent>

                <ErrorPComponent>
                    {pComponent}
                </ErrorPComponent>

                <BtnComponent>
                    {btn}
                </BtnComponent>


            </ErrorComponent>
            <JSLogoComponent src={require('../../../../img/entry/js_corp.png')}/>
        </BoxWrapComponent>
    )
};

const mapStateToProps = (store) => {
    return {
        errorForm: store.errorForm
    }
};

export default connect(mapStateToProps)(ErrorContainer);
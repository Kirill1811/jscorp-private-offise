import React, { Component } from 'react';
import {connect} from 'react-redux';
import BoxWrapComponent from '../../../components/Form/BoxWrapComponent';
import {Link} from 'react-router-dom';
import submit from '../../../../utils/submit/recoverySubmit'

class RecoveryContainer extends Component {

    render() {
        const {
            titleComponent,
            fields,
            btnComponent,
            infoComponents,
            formName,
        } = this.props.recovery;

        const info = <Link to={infoComponents.linkTo}>{infoComponents.linkText}</Link>;

        return (
            <BoxWrapComponent
                handleSubmit={submit}
                titleComponent={titleComponent}
                fields={fields}
                btnComponent={btnComponent}
                formName={formName}
                info={info}
            />
        )
    }
}

const mapStateToProps = store => {
    return {
        recovery: store.recovery
    }
};

export default connect(mapStateToProps)(RecoveryContainer);
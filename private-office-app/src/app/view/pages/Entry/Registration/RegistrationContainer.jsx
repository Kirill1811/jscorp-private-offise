import React, { Component } from 'react';
import { connect } from 'react-redux'
import BoxWrapComponent from '../../../components/Form/BoxWrapComponent';
import CheckboxComponent from '../../../components/Form/CheckboxComponent';
import submit from '../../../../utils/submit/regSubmit';


class RegistrationContainer extends Component {

    render() {
        const {
            titleComponent,
            fields,
            checkboxesTitle,
            checkboxes,
            mainCheckbox,
            btnComponent,
            formName,
        } = this.props.registration;

        const info = <CheckboxComponent
            checkboxesTitle={checkboxesTitle}
            checkboxes={checkboxes}
            mainCheckbox={mainCheckbox}
        />;

        return (
            <BoxWrapComponent
                handleSubmit={submit}
                titleComponent={titleComponent}
                fields={fields}
                btnComponent={btnComponent}
                formName={formName}
                info={info}
            />
        )
    }
}

const mapStateToProps = store => {
    return {
        registration: store.registration
    }
};

export default connect(mapStateToProps)(RegistrationContainer);
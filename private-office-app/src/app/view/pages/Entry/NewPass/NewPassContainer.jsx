import React, { Component } from 'react';
import {connect} from "react-redux";
import BoxWrapComponent from '../../../components/Form/BoxWrapComponent';
import {Link} from 'react-router-dom';
import submit from '../../../../utils/submit/newPassSubmit';

class NewPassContainer extends Component {

    render() {
        const {
            titleComponent,
            fields,
            btnComponent,
            infoComponents,
            formName,
        } = this.props.new_pass;

        const info = <Link to={infoComponents.linkTo}>{infoComponents.linkText}</Link>;

        return (
            <BoxWrapComponent
                handleSubmit={submit}
                titleComponent={titleComponent}
                fields={fields}
                btnComponent={btnComponent}
                formName={formName}
                info={info}
            />
        )
    }
}

const mapStateToProps = store => {
    return {
        new_pass: store.new_pass
    }
};

export default connect(mapStateToProps)(NewPassContainer);
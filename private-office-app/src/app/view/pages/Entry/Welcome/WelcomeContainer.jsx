import React from 'react';
import {connect} from 'react-redux';
import styled from 'styled-components';

const BoxWrapComponent = styled.div`
  margin: 186px auto;
  width: 100%;
  max-width: 396px;
  
  @media (max-width: 1199.98px) {
    max-width: 280px;
    margin: 150px auto;
  }
  
  @media (max-width: 575.98px) {
    max-width: 90%;
    margin: 10px auto;
  }
`;

const WelcomeComponent = styled.div`
  margin: 286px auto 45px;
  width: 100%;
  max-width: 100%;
  -webkit-box-shadow: 1px 1px 8px #ccc;
          box-shadow: 1px 1px 8px #ccc;
  padding: 18px 40px;
  border-radius: 10px;
  position: relative;
  
  ::before {
      content: "";
      background-image: url(${require('../../../../img/entry/welcome.png')});
      background-repeat: no-repeat;
      background-size: 130%;
      display: block;
      width: 100%;
      max-width: 120px;
      min-height: 156px;
      position: absolute;
      top: -80px;
      left: 50%;
      -webkit-transform: translate(-50%, -50%);
          -ms-transform: translate(-50%, -50%);
              transform: translate(-50%, -50%);
      overflow: hidden;
  }
`;

const WelcomeTitleComponent = styled.h3`
  margin: 0;
  color: #0eb8cc;
  text-align: center;
  font-family: "GhothemProBlack", sans-serif;
`;

const WelcomePComponent = styled.p`
  text-align: center;
  font-size: 14px;
  latter-spacing: 1.4;
`;

const BtnActiveComponent = styled.div`
  color: #fff;
  margin-bottom: 16px;
  background: #0eb8cc;
  padding: 10px 15px;
  width: 100%;
  display: block;
  text-align: center;
  border-radius: 10px;
  cursor: pointer;
  
  :hover {
     background: #009aae;
     color: #fff;
     text-decoration: none;
  }
`;

const BtnPassiveComponent = styled.div`
  color: #fff;
  background: #cfcfcf;
  padding: 10px 15px;
  width: 100%;
  display: block;
  text-align: center;
  border-radius: 10px;
  cursor: pointer;
  
  :hover {
     background: #b1b1b1;
     color: #fff;
     text-decoration: none;
  }
`;

const JSLogoComponent = styled.img`
  display: block;
  width: 50%;
  max-width: 250px;
  margin: 25px auto;
`;

 const WelcomeContainer = (props) => {
    const {
        titleComponent,
        pComponents,
        btns
    } = props.welcomeForm;

    const info = pComponents.map((pComponent, index) => {
        return <WelcomePComponent key={index}>{pComponent}</WelcomePComponent>
    });

    return (
        <BoxWrapComponent>
            <WelcomeComponent>
                <WelcomeTitleComponent>
                    {titleComponent}
                </WelcomeTitleComponent>

                {info}

                <BtnActiveComponent>
                    {btns[0]}
                </BtnActiveComponent>

                <BtnPassiveComponent>
                    {btns[1]}
                </BtnPassiveComponent>
            </WelcomeComponent>
            <JSLogoComponent src={require('../../../../img/entry/js_corp.png')}/>
        </BoxWrapComponent>
    )
};

const mapStateToProps = (store) => {
    return {
        welcomeForm: store.welcomeForm
    }
};

export default connect(mapStateToProps)(WelcomeContainer);
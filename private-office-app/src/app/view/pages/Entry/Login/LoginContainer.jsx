import React, { Component } from 'react';
import { connect } from 'react-redux'
import { Link } from 'react-router-dom';
import BoxWrapComponent from '../../../components/Form/BoxWrapComponent';
import submit from '../../../../utils/submit/loginSubmit';

class LoginContainer extends Component {

    render() {
        const {
            titleComponent,
            fields,
            btnComponent,
            infoComponents,
            formName,
        } = this.props.login;

        const info = infoComponents.map((infoComponent, index) => {
            return <Link key={index} to={infoComponent.linkTo}>{infoComponent.linkText}</Link>
        });

        return (
            <BoxWrapComponent
                handleSubmit={submit}
                titleComponent={titleComponent}
                fields={fields}
                btnComponent={btnComponent}
                formName={formName}
                info={info}
            />
        )
    }
}

const mapStateToProps = store => {
    return {
        login: store.login
    }
};

export default connect(mapStateToProps)(LoginContainer);
import React, { Component } from 'react';
import { connect } from 'react-redux';
import Header from '../../components/PrOffice/Header/Header';
import Menu from '../../components/PrOffice/Menu/Menu';
//import Profile from '../../components/PrOffice/Profile/Profile';

import state from '../../../siteState';

import { showMenu, showUserMenu } from '../../../actions/UserOfficeActions';

class UserOffice extends Component {
	render() {
		const siteState = state;

		const {
			changMarker,
			showUserMenu,
			userMenuPoints,
			menuPoints,
		} = this.props.userOffice;

		return (
			<div>
				<Header
					siteState={siteState}
					showMenu={this.props.showMenuAction}
					marker={changMarker}
					userMenuPoints={userMenuPoints}
					showUserMenu={this.props.showUserMenuAction}
					userMenuMarker={showUserMenu}
				/>
				<Menu show={changMarker} points={menuPoints} />
			</div>
		);
	}
}

const mapStateToProps = store => {
	return {
		userOffice: store.userOffice,
	};
};

const mapDispatchToProps = dispatch => ({
	showMenuAction: changMarker => dispatch(showMenu(changMarker)),
	showUserMenuAction: show => dispatch(showUserMenu(show)),
});

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(UserOffice);

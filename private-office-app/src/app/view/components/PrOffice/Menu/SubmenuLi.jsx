import React from 'react';
import styled from 'styled-components';
import {Link} from "react-router-dom";

const NavMenuLi = styled.li`
  padding: 10px 24px 10px 50px;
  list-style: none;
  cursor: pointer;
  
  :hover {
    background-color: #0eb8cc;
    transition: background-color 1.1s ease;
  }
  
  @media screen and (max-width: 640px) {
    padding: 10px 30px;
  }
`;

const NavMenuLiLink = styled(Link)`
  color: #fff;
  position: relative;
  
  :hover {
    color: #fff;
    text-decoration: none;
  }
  
  ::before {
    content: "";
    display: block;
    background-repeat: no-repeat;
    background-size: 12px;
    color: #fff;
    width: 12px;
    height: 12px;
    position: absolute;
    top: 50%;
    left: -25px;
    -webkit-transform: translateY(-50%);
       -ms-transform: translateY(-50%);
          transform: translateY(-50%);
    cursor: pointer;
  }
`;

const MyProfile = styled(NavMenuLiLink)`
  ::before {
    background-image: url(${require('../../../../img/pr_office/header&menu/My-profile_11.png')});
  }
`;

const MyProjects = styled(NavMenuLiLink)`
  ::before {
    background-image: url(${require('../../../../img/pr_office/header&menu/projeckt_5.png')});
  }
`;

const MyDocuments = styled(NavMenuLiLink)`
  ::before {
    background-image: url(${require('../../../../img/pr_office/header&menu/My-document_2.png')});
  }
`;

const CodingProgram = styled(NavMenuLiLink)`
  ::before {
    background-image: url(${require('../../../../img/pr_office/header&menu/Coding-program.png')});
  }
`;

const DesignObjects = styled(NavMenuLiLink)`
  ::before {
    background-image: url(${require('../../../../img/pr_office/header&menu/photo-library.png')});
  }
`;

const Specialists = styled(NavMenuLiLink)`
  ::before {
    background-image: url(${require('../../../../img/pr_office/header&menu/specialist_3.png')});
  }
`;

const Innovators = styled(NavMenuLiLink)`
  ::before {
    background-image: url(${require('../../../../img/pr_office/header&menu/innovators_4.png')});
  }
`;

const Projects = styled(NavMenuLiLink)`
  ::before {
    background-image: url(${require('../../../../img/pr_office/header&menu/projeckt_5.png')});
  }
`;

const Mentors = styled(NavMenuLiLink)`
  ::before {
    background-image: url(${require('../../../../img/pr_office/header&menu/mentor_7.png')});
  }
`;

const Curators =styled(NavMenuLiLink)`
  ::before {
    background-image: url(${require('../../../../img/pr_office/header&menu/curators_6.png')});
  }
`;

const Intership = styled(NavMenuLiLink)`
  ::before {
    background-image: url(${require('../../../../img/pr_office/header&menu/intership_8.png')});
  }
`;

const Applications = styled(NavMenuLiLink)`
  ::before {
    background-image: url(${require('../../../../img/pr_office/header&menu/applications_9.png')});
  }
`;

const Support = styled(NavMenuLiLink)`
  ::before {
    background-image: url(${require('../../../../img/pr_office/header&menu/support_13.png')});
  }
`;

const Contacts = styled(NavMenuLiLink)`
  ::before {
    background-image: url(${require('../../../../img/pr_office/header&menu/netbool-contactpng.png')});
  }
`;

const Global = styled(NavMenuLiLink)`
  ::before {
    background-image: url(${require('../../../../img/pr_office/header&menu/global.png')});
  }
`;

const SubmenuLi = (props) => {
    const { point } = props;

    const menuPoint = (point) => {
        switch (point) {
            case 'Мой профиль':
                return <MyProfile>{ point }</MyProfile>;

            case 'Мои Проекты':
                return <MyProjects>{ point }</MyProjects>;

            case 'Мои документы':
                return <MyDocuments>{ point }</MyDocuments>;

            case 'Исходный код':
                return <CodingProgram>{ point }</CodingProgram>;

            case 'Объекты дизайна':
                return <DesignObjects>{ point }</DesignObjects>;

            case 'Специалисты':
                return <Specialists>{ point }</Specialists>;

            case 'Новаторы':
                return <Innovators>{ point }</Innovators>;

            case 'Проекты':
                return <Projects>{ point }</Projects>;

            case 'Кураторы':
                return <Curators>{ point }</Curators>;

            case 'Менторы':
                return <Mentors>{ point }</Mentors>;

            case 'Стажировка':
                return <Intership>{ point }</Intership>;

            case 'Заявки':
                return <Applications>{ point }</Applications>;

            case 'Поддержка':
                return <Support>{ point }</Support>;

            case 'Контакты':
                return <Contacts>{ point }</Contacts>;

            default:
                return <Global>{ point }</Global>

        }
    };

    return (
        <NavMenuLi>
            { menuPoint(point) }
        </NavMenuLi>
    )
};

export default SubmenuLi;
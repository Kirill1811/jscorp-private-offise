import React from 'react';
import styled from 'styled-components';
import Submenu from './Submenu';

const Navigation = styled.div`
  position: fixed;
  margin-top: 96px;
  width: 300px;
  height: calc(100vh - 96px);
  z-index: ${props => props.show ? '99' : '-1'};
  transition: .3s .200s ease;
  overflow-y: ${props => props.show ? 'scroll' : 'hidden'};
  
  ::-webkit-scrollbar {
    width: 8px;
  }
  
  @media screen and (max-width: 640px) {
    top: -2px;
    width: 100vw;
  }
  
  @media screen and (max-width: 320px)  {
    top: -12px;
  }
`;

const NavInner = styled.div`
  position: absolute;
  width: 300px;
  top: 0;
  background-color: #20242c;
  z-index: 999999;
  transform: translateX(${props =>  props.show ? 0 : -300}px);
  transition: transform 300ms linear, background-color 0s linear 599ms;
  border-radius: ${props => props.show ? '50%' : '0'};
  animation: ${props =>  props.show ? 'elastic 200ms ease 200.5ms both' : 'none'};
  
  @keyframes elastic {
  0% {
    border-top-left-radius: 0;
    border-bottom-left-radius: 0; }
  45% {
    border-radius: 0; }
  65% {
    border-top-right-radius: 40px 50%;
    border-bottom-right-radius: 40px 50%; }
  80% {
    border-radius: 0; }
  90% {
    border-top-right-radius: 20px 50%;
    border-bottom-right-radius: 20px 50%; }
  100% {
    border-radius: 0; } }
  
`;

const NavMenu = styled.div`
  font-size: 15px;
  -webkit-transition: opacity 0.5s ease 0.3s;
  -o-transition: opacity 0.5s ease 0.3s;
  transition: opacity 0.5s ease 0.3s;
  opacity: ${props => props.show ? '1' : '0'};
`;

const Section = styled.h4`
  font-weight: 600;
  color: #0eb8cc;
  padding: 5px 24px;
  margin: 10px 0 0 0;
  
  @media screen and (max-width: 640px) {
    margin: 20px 30px;
  }
`;

const Menu = (props) => {
    const {
        show,
        points
    } = props;

    const menuPoints = points.map((point, index) => {
        return (typeof (point) === "string") ?
            <Section key={index}>{point}</Section> :
            <Submenu key={index} points={point}/>
    });

    return (
        <Navigation show={show}>
            <NavInner show={show}>
                <NavMenu show={show}>
                    {menuPoints}
                </NavMenu>
            </NavInner>
        </Navigation>
    );
};


export  default Menu;
import React from 'react';
import styled from 'styled-components';
import SubmenuLi from './SubmenuLi';

const NavMenuUl = styled.ul`
  margin 0;
  padding: 0;
`;



const Submenu = (props) => {
    const { points } = props;
    
    const submenuPoints = points.map((point, index) => {
        return <SubmenuLi key={index} point={point}/>
    });

    return (
        <NavMenuUl>
            { submenuPoints }
        </NavMenuUl>
    )
};

export default Submenu;
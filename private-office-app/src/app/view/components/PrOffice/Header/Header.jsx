import React from 'react';
import styled from 'styled-components';
import LogoContainer from './LogoContainer';
import WrappNav from './WrappNav';
import UserNav from './UserNav';

const HeaderBox = styled.div`
  position: fixed;
  z-index: 999;
  width: 100%;
  background: #121519;
  padding: 10px 24px 12px;
  
  @media screen and (max-width: 640px) {
    padding: 11px 0;
  }
`;

const HeaderContainer = styled.div`
  display: -webkit-box;
  display: -webkit-flex;
  display: -ms-flexbox;
  display: flex;
  padding: 10px 24px 12px;
`;

const Nav = styled.div`
  margin-left: 20px;
  display: -webkit-box;
  display: -webkit-flex;
  display: -ms-flexbox;
  display: flex;
  -webkit-box-align: center;
  -webkit-align-items: center;
      -ms-flex-align: center;
          align-items: center;
  -webkit-box-pack: justify;
  -webkit-justify-content: space-between;
      -ms-flex-pack: justify;
          justify-content: space-between;
  width: 100%;
  
  @media (max-width: 991.98px){
    flex-direction: row-reverse;
    -webkit-box-pack: end;
    -webkit-justify-content: flex-end;
        -ms-flex-pack: end;
            justify-content: flex-end;
  }
`;

const Header = (props) => {
    const {
        siteState,
        showMenu,
        marker,
        userMenuPoints,
        showUserMenu,
        userMenuMarker
    } = props;

    return (
        <HeaderBox>
            <HeaderContainer>

                <LogoContainer
                    active={marker}
                    showMenu={showMenu}
                />

                <Nav>

                    <WrappNav siteState={siteState[0].roles}/>

                    <UserNav
                        user={siteState[0].userData.name}
                        userMenuPoints={userMenuPoints}
                        showUserMenu={showUserMenu}
                        userMenuMarker={userMenuMarker}
                    />

                </Nav>

            </HeaderContainer>
        </HeaderBox>
    )
};

export default Header;
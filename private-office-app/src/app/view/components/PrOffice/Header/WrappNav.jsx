import React from 'react';
import styled from 'styled-components';
import {Link} from "react-router-dom";

const WrappNav = styled.div`
  @media screen and (max-width: 1320px){
    display: none;
  }
`;

const NavUl = styled.ul`
  position: absolute;
  top: 35%;
  display: -webkit-box;
  display: -webkit-flex;
  display: -ms-flexbox;
  display: flex;
  max-width: 795px;
  -webkit-flex-wrap: wrap;
      -ms-flex-wrap: wrap;
          flex-wrap: wrap;
  -webkit-box-align: center;
  -webkit-align-items: center;
      -ms-flex-align: center;
          align-items: center;
`;

const NavLi = styled.li`
  position: relative;
  padding: 0 10px;
  color: #fff;
  margin: 0 10px;
  font-size: 15px;
  list-style-type: none;
  
  ::before {
      content: "";
      display: block;
      background-repeat: no-repeat;
      background-size: 12px;
      color: #fff;
      width: 12px;
      height: 12px;
      position: absolute;
      top: 45%;
      left: -10px;
      -webkit-transform: translateY(-50%);
          -ms-transform: translateY(-50%);
              transform: translateY(-50%);
      cursor: pointer;
       -webkit-box-sizing: border-box;
          box-sizing: border-box;
  }
`;

const LiSpecialists = styled(NavLi)`
  ::before {
    background-image: url(${require('../../../../img/pr_office/header&menu/specialist_3.png')});
  }
`;

const LiInnovators = styled(NavLi)`
  ::before {
    background-image: url(${require('../../../../img/pr_office/header&menu/innovators_4.png')});
  }
`;

const LiProjects = styled(NavLi)`
  ::before {
    background-image: url(${require('../../../../img/pr_office/header&menu/projeckt_5.png')});
  }
`;

const LiCurators = styled(NavLi)`
  ::before {
    background-image: url(${require('../../../../img/pr_office/header&menu/curators_6.png')});
  }
`;

const LiMentors = styled(NavLi)`
  ::before {
    background-image: url(${require('../../../../img/pr_office/header&menu/mentor_7.png')});
  }
`;

const Spic = styled.span`
  color: #0eb8cc;
`;

const StyledLink = styled(Link)`
  :hover {
    color: #fff;
  }
`;

const Wrapper = (props) => {
    const { siteState } = props;

    const roles = siteState.map((item, index) => {
        switch (item.role) {
            case 'specialists':
                return (
                    <StyledLink
                        key={index}
                        to={'#'}
                    >
                        <LiSpecialists>
                            {'Специалисты: '}
                            <Spic>
                                {item.strength}
                            </Spic>
                        </LiSpecialists>
                    </StyledLink>
                );

            case 'innovators':
                return (
                    <StyledLink
                        key={index}
                        to={'#'}
                    >
                        <LiInnovators>
                            {'Новаторы: '}
                            <Spic>
                                {item.strength}
                            </Spic>
                        </LiInnovators>
                    </StyledLink>
                );

            case 'projects':
                return (
                    <StyledLink
                        key={index}
                        to={'#'}
                    >
                        <LiProjects>
                            {'Проекты: '}
                            <Spic>
                                {item.strength}
                            </Spic>
                        </LiProjects>
                    </StyledLink>
                );

            case 'curators':
                return (
                    <StyledLink
                        key={index}
                        to={'#'}
                    >
                        <LiCurators>
                            {'Кураторы: '}
                            <Spic>
                                {item.strength}
                            </Spic>
                        </LiCurators>
                    </StyledLink>
                );

            default:
                return (
                    <StyledLink
                        key={index}
                        to={'#'}
                    >
                        <LiMentors>
                            {'Менторы: '}
                            <Spic>
                                {item.strength}
                            </Spic>
                        </LiMentors>
                    </StyledLink>
                );
        }
    });

    return (
        <WrappNav>
            <NavUl>
                {roles}
            </NavUl>
        </WrappNav>
    )
};

export default Wrapper;
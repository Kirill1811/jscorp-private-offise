import React from 'react';
import styled from 'styled-components';
import {Link} from "react-router-dom";

const NavUser = styled.div`
  display: -webkit-box;
  display: -webkit-flex;
  display: -ms-flexbox;
  display: flex;
  -webkit-box-align: center;
  -webkit-align-items: center;
      -ms-flex-align: center;
          align-items: center;
  -webkit-justify-content: space-around;
      -ms-flex-pack: distribute;
          justify-content: space-around;
  -webkit-flex-basis: 175px;
      -ms-flex-preferred-size: 175px;
          flex-basis: 175px;
`;

const Notice = styled.img`
  height: 20px;
  margin-right:  10px;
  cursor: pointer;
`;

const Help = styled.img`
  height: 20px;
  margin-right:  10px;
  cursor: pointer;
  
  @media screen and (max-width: 640px) {
    display: none;
  }
`;

const User = styled.div`
  display: -webkit-box;
  display: -webkit-flex;
  display: -ms-flexbox;
  display: flex;
  -webkit-box-align: center;
  -webkit-align-items: center;
      -ms-flex-align: center;
          align-items: center;
  
  ::after {
      content: '';
      border-left: 5px solid transparent;
      border-top: 5px solid ${props =>  props.userMenuMarker ? '#fff' : 'transparent'};
      border-right: 5px solid transparent;
      border-bottom: 5px solid ${props =>  props.userMenuMarker ? 'transparent' : '#fff'};
      height: 10px;
      margin-left: 5px;
      margin-bottom: ${props =>  props.userMenuMarker ? -5 : 5}px;
      cursor: pointer;
  }
  
  @media screen and (max-width: 640px)  {
    display: none
  }
`;

const UserLogo = styled.div`
  height: 35px;
  width: 35px;
  border-radius: 100%;
  border: 3px solid #f3f4f6;
  background: #0eb8cc;
`;

const UserName = styled.div`
  margin-left: 8px;
  font-size: 15px;
  color: #fff;
`;

const UserMenu = styled.div`
    position: absolute;
    width: 220px;
    top: 96px;
    right: 0;
    background: #121519;
`;

const UserMenuItem = styled.div`
    height: ${props =>  props.userMenuMarker ? 60 : 0}px;
    opacity: ${props =>  props.userMenuMarker ? 1 : 0};
    transition: height 0.3s, opacity 0.4s;
`;

const UserMenuItemSpan = styled(Link)`
    width: 100%;
    padding: 15px 15px;
    display: inline-block;
    color: #fff;
    text-decoration: none; 
    height: inherit;
    overflow: hidden;
    opacity: ${props =>  props.userMenuMarker ? 1 : 0};
    transition: opacity 0.6s;
    
    :hover {
      color: #fff;
      text-decoration: none;
      background: #0eb8cc;
    }
`;

const UserNav = (props) => {
    const { user,
        userMenuPoints,
        showUserMenu,
        userMenuMarker
    } = props;

    const changMarker = () => {
        showUserMenu(!userMenuMarker);
    };

    const menuPoints = userMenuPoints.map((point, index) => {
        return (
            <UserMenuItem
                key={index}
                userMenuMarker={userMenuMarker}
            >
                <UserMenuItemSpan userMenuMarker={userMenuMarker}>{point}</UserMenuItemSpan>
            </UserMenuItem>
        )
    });

    return (
        <NavUser>
            <Notice src={require('../../../../img/pr_office/header&menu/notification.png')}/>

            <Help src={require('../../../../img/pr_office/header&menu/help.png')}/>

            <User
                onClick={changMarker}
                userMenuMarker={userMenuMarker}
            >
                <UserLogo/>

                <UserName>
                    {user.split(' ')[1]}
                </UserName>

                <UserMenu>{menuPoints}</UserMenu>
            </User>

        </NavUser>
    )
};

export default UserNav;
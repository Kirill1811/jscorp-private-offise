import React from 'react';
import styled from 'styled-components';

const LogoContainer = styled.div`
  position: relative;
  width: 100%;
  max-width: 230px;
  cursor: pointer;
  
  ::before {
      content: "";
      display: block;
      background-image: url(${props => props.active ? require('../../../../img/pr_office/header&menu/Menu.png') :
    require('../../../../img/pr_office/header&menu/White_Menu.png')});
      background-repeat: no-repeat;
      background-size: 12px;
      color: #fff;
      width: 12px;
      height: 12px;
      position: absolute;
      top: 50%;
      left: 95%;
      -webkit-transform: translateY(-50%);
          -ms-transform: translateY(-50%);
              transform: translateY(-50%);
  }
  
  @media screen and (max-width: 640px) {
    ::before {
      left: calc(100vw - 70px);
    }
  }
`;

const LogoImg = styled.img`
  display: block;
  max-width: 190px;
  
  @media screen and (max-width: 640px) {
    height: 50px;
  }
  
  @media screen and (max-width: 320px) {
    height: 40px;
  }
`;

const LogoCont = (props) => {
    const {
        active,
        showMenu
    } = props;

    const changMarker = () => {
        showMenu(!active);
    };

    return (
        <LogoContainer active={active} onClick={changMarker}>
            <LogoImg src={require('../../../../img/pr_office/header&menu/logo-2.png')}/>
        </LogoContainer>
    )
};

export default LogoCont;
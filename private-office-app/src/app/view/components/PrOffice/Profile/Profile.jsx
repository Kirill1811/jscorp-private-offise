import React from 'react';
import styled from 'styled-components';
import ProfileContainer from './ProfileContainer';
import InfoContainer from './InfoContainer';
import SkillsContainer from './SkillsContainer';

const Padding = styled.main``;

const Container = styled.form``;

const SupportLine = styled.div``;

const EditBtn = styled.button``;

const ExportBtn = styled.button``;

const ProfileInfo = styled.div``;

const About = styled(ProfileContainer)``;




const Profile = props => {
    const {
        siteState,
        showMenu,
        profileTemplate
    } = props;

    return (
        <Padding>
            <Container>
                <SupportLine>
                    <EditBtn/>
                    <ExportBtn/>
                </SupportLine>
                <ProfileInfo></ProfileInfo>
                <About></About>
            </Container>
        </Padding>
    )
};

export default Profile;
import React, { Component } from 'react';
import styled from 'styled-components';
import { compose } from 'redux';
import { connect } from 'react-redux';
import { Field, reduxForm } from 'redux-form';
import { validate } from '../../../utils/control/formValidate';

const CheckInFormInputComponent = styled.input`
  display: -webkit-box;
  display: -webkit-flex;
  display: -ms-flexbox;
  display: flex;
  -webkit-box-orient: vertical;
  -webkit-box-direction: normal;
  -webkit-flex-direction: column;
  -ms-flex-direction: column;
  flex-direction: column;
  width: 100%;
  padding: 5px 15px;
  border-radius: 10px;
  margin-bottom: ${props =>(props.hasError ? '-10px' : '18px')};
  font-size: 1em;
  box-sizing: border-box;
  border: ${props =>(props.hasError ? '2px solid #DC143C' : '1px solid #949494')};
  
  :focus {
    border: 1px solid #0eb8cc;
  }

  @media (max-width: 1199.98px) {
    font-size: 0.8em;
    padding: 6px 10px;
    margin-bottom: 11px;
  }
  
   @media (max-width: 575.98px) {
        margin-bottom: 18px;
   }
`;

const TipBlock = styled.div`
  display: inline-block;
  position: relative;
  background: transparent;
  margin: 0;
  padding: 0;
  border: 0;
  height: 0;
  width: 1px;
  
  @media (max-width: 575.98px) {
    display: block;
    position: relative;
    margin: 15px 0;
    height: auto;
    width: 100%;
  }
`;

const TipBlockInside = styled.div`
  @media (min-width: 576px) {
      display: ${props =>(props.activeField ? 'none' : 'block')};
      position: absolute;
      z-index: 9998;
      top: 0;
      left: 100%;
      background: #F08080;
      border-radius: 3px;
      border: 1px solid #DC143C;
      box-shadow: 5px 5px 0.5em -0.1em rgba(0, 0, 6, 0.5);
      text-align: left;
      color: #FFFFFF;
      font-size: 1em;
      opacity: .9;
      cursor: default;
      padding: 5px;
      width: 350px;
      height: auto;
      box-sizing: border-box;
      margin: -55px 0 5px 325px;
      -webkit-animation: fadeIn 2s;
      animation: fadeIn 2s
      
      ::before {
          border-width: 9px 8px 9px 0;
          border-color: transparent #DC143C transparent transparent;
          top: 13px;
          left: -9px;
      }
      
      ::after {
          border-width: 7px 7px 7px 0;
          border-color: transparent #F08080 transparent transparent;
          top: 15px;
          left: -7px;
      }
      
      ::before,
      ::after {
        content: "";
        position: absolute;
        width: 0;
        height: 0;
        border-style: solid;
      }
  }
  
  @media (max-width: 575.98px) {
      position: relative;
      display: ${props =>(props.activeField ? 'none' : 'block')};
      top: 0;
      left: 0;
      background: #F08080;
      border-radius: 3px;
      box-shadow: 5px 5px 0.5em -0.1em rgba(0, 0, 6, 0.5);
      text-align: left;
      color: #FFFFFF;
      font-size: 0.8em;
      opacity: .9;
      cursor: default;
      padding: 5px;
      width: 100%;
      min-height: 35px;
      height: auto;
      -webkit-animation: fadeIn 2s;
      animation: fadeIn 2s
      
      ::before {
          content: "";
          position: absolute;
          width: 0;
          height: 0;
          border: 15px solid transparent; border-bottom: 15px solid #F08080;
          top: -30px;
          left: 50%;
      }
  }

  @-webkit-keyframes fadeIn {
    from {opacity: 0;}
    to {opacity: 1;}
  }

  @keyframes fadeIn {
    from {opacity: 0;}
    to {opacity:1 ;}
  }
`;

const AddInfBtnComponent = styled.button`
  color: #fff;
  background: #0eb8cc;
  padding: 10px 15px;
  width: 100%;
  display: block;
  text-align: center;
  border-radius: 10px;
  cursor: pointer;
  border: 0;
  outline: 0 !important;
  
  ::-moz-focus-outer, ::-moz-focus-inner {
    border: 0;
    padding: 0;
  }
  
  :hover {
    background: #009aae;
    color: #fff;
    text-decoration: none;
  }
  
  @media (max-width: 1199.98px){
    font-size: 0.8em;
    padding: 6px 10px;
    width: 100%;
  }
`;

const InfoComponent = styled.div`
  text-align: center;
  margin: 10px 0;
`;

class FormBlock extends Component {
    renderField = ({
                       input,
                       label,
                       type,
                       meta: {touched, active, error}
                   }) => {
        const hasError = touched && error;

        return (
            <div>
                <CheckInFormInputComponent
                    {...input} hasError={hasError} placeholder={label} type={type}/>
                {hasError && (
                    <TipBlock>
                        <TipBlockInside activeField={active}>{error}</TipBlockInside>
                    </TipBlock>
                )}

            </div>
        );
    };

    render() {
        const {
            error,
            handleSubmit,
            fields,
            btnComponent,
            info,
            pristine,
            submitting
        } = this.props;

        const formFields = fields.map((field, index) => {
            return (
                <Field
                    key={index}
                    name={field.name}
                    label={field.label}
                    type={field.type}
                    normalize={field.normalize.length > 0 ? field.normalize[0] : undefined}
                    component={this.renderField}
                />
            );
        });
        return (
            <div>
                <form onSubmit={handleSubmit}>

                    {formFields}

                    <InfoComponent>
                        {info}
                    </InfoComponent>

                    <AddInfBtnComponent disabled={pristine || submitting}>
                        {btnComponent}
                    </AddInfBtnComponent>
                </form>
                {
                    error &&
                    <TipBlock>
                        <TipBlockInside>{error}</TipBlockInside>
                    </TipBlock>
                }
            </div>
        )
    }
}

const mapStateToProps = (state, ownProps) => ({
    form: ownProps.formName,
});

export default compose(
    connect(mapStateToProps),
    reduxForm({
        validate
    })
)(FormBlock);
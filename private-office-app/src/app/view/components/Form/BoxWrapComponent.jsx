import React, { Component } from 'react';
import styled from 'styled-components';
import FormBlock from './FormBlock';

const BoxWrapComponent = styled.div`
  margin: 186px auto;
  width: 100%;
  max-width: 396px;
  
  @media (max-width: 1199.98px) {
    max-width: 280px;
    margin: 150px auto;
  }
  
  @media (max-width: 575.98px) {
    max-width: 90%;
    margin: 10px auto;
  }
`;

const FormContainer = styled.div`
  margin: 286px auto 45px;
  width: 100%;
  padding: 18px 40px;
  -webkit-box-shadow: 1px 1px 8px #ccc;
          box-shadow: 1px 1px 8px #ccc;
  padding: 18px 40px;
  border-radius: 10px;
  position: relative;
  
  ::before {
      content: "";
      background-repeat: no-repeat;
      background-size: 100%;
      display: block;
      width: 100%;
      position: absolute;
      top: -80px;
      left: 50%;
      -webkit-transform: translate(-50%, -50%);
          -ms-transform: translate(-50%, -50%);
              transform: translate(-50%, -50%);
      overflow: hidden;
  }
  
  @media (max-width: 1199.98px) {
      padding: 0px 26px 18px;
  }
  
  @media (max-width: 575.98px) {
      ::before {
        background-image: none;
      }  
  }
`;

const LoginComponent = styled(FormContainer)`
  ::before {
    background-image: url(${require('../../../img/entry/login.png')});
    max-width: 180px;
    min-height: 156px;
  }
`;

const NewPass = styled(FormContainer)`
  ::before {
    background-image: url(${require('../../../img/entry/recovery_2.png')});
    max-width: 160px;
    min-height: 185px;
  }
`;

const Recovery = styled(FormContainer)`
  ::before {
    background-image: url(${require('../../../img/entry/recovery.png')});
    max-width: 160px;
    min-height: 160px;
  }
`;

const Registration = styled(FormContainer)`
  ::before {
    background-image: url(${require('../../../img/entry/fox_reg.png')});
    max-width: 103px;
    min-height: 195px;
  }
`;

const FormTitleComponent = styled.h2`
  margin: 0 0 10px 0;
  color: #0eb8cc;
  text-align: center;
  font-family: "GhothemProBlack", sans-serif;
`;

const CheckInFormComponent = styled.form`
    box-sizing: border-box;
`;

const JSLogoComponent = styled.img`
  display: block;
  width: 50%;
  max-width: 250px;
  margin: 25px auto;
`;

class BoxWrap extends Component {
    formContainerComponent = (formName) => {
        switch (formName) {
            case 'new_pass':
                return NewPass;

            case 'recovery':
                return Recovery;

            case 'registration':
                return Registration;

            default:
                return LoginComponent;
        }
    };

    render() {
        const {
            handleSubmit,
            titleComponent,
            fields,
            btnComponent,
            formName,
            info
        } = this.props;

        return (
            <BoxWrapComponent>
                <FormContainer as={this.formContainerComponent(formName)}>

                    <FormTitleComponent>
                        {titleComponent}
                    </FormTitleComponent>

                    <CheckInFormComponent>
                        <FormBlock
                            onSubmit={handleSubmit}
                            fields={fields}
                            btnComponent={btnComponent}
                            formName={formName}
                            info={info}
                        />

                    </CheckInFormComponent>

                </FormContainer>

                <JSLogoComponent src={require('../../../img/entry/js_corp.png')}/>
            </BoxWrapComponent>
        )
    }
}

export default BoxWrap;
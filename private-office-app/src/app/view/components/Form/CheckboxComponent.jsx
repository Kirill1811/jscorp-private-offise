import React, { Component } from 'react';
import styled from 'styled-components';
import { Field } from 'redux-form';
import {Link} from "react-router-dom";

const AddInfTitleComponent = styled.div`
    text-align: left;
    margin-bottom: 16px;
`;

const AddInfTitleSpanComponent = styled.span`
    font-family: "GhothemProBlack", sans-serif;
    position: relative;
    vertical-align: middle;
    
    ::after {
      position: absolute;
      top: 50%;
      -webkit-transform: translateY(-50%);
          -ms-transform: translateY(-50%);
              transform: translateY(-50%);
      left: 105%;
      content: url(${require('../../../img/entry/how_are_you.png')});
      width: 20px;
      height: 20px;
      display: inline-block;
    }
`;

const AddInfCheckboxMainComponent = styled.label`
  margin: 15px 0 10px;
  min-width: 80px;
  font-size: 1em;
  text-align: left;
  position: relative;
  display: -webkit-box;
  display: -webkit-flex;
  display: -ms-flexbox;
  display: block;
  -webkit-box-align: center;
  -webkit-align-items: center;
      -ms-flex-align: center;
          align-items: center;
  cursor: pointer;
  line-height: normal;
  
  
  @media (max-width: 1199.98px){
    margin: 5px 0 8px;
    min-width: 60px;
    font-size: 0.8em;
  }
`;

const AddInfCheckboxInput = styled.input`
    display: none;
    
   :checked + label::before {
      background: #0eb8cc;
      border: 1px solid #0eb8cc;
      -webkit-animation: click-wave .5s;
              animation: click-wave .5s;
    }
`;

const AddInfCheckboxInputLabelComponent = styled.label`
  margin: 0  0  0 35px;
  min-width: 80px;
  font-size: 1em;
  text-align: left;
  position: relative;
  display: -webkit-box;
  display: -webkit-flex;
  display: -ms-flexbox;
  display: flex;
  -webkit-box-align: center;
  -webkit-align-items: center;
      -ms-flex-align: center;
          align-items: center;
  cursor: pointer;
  
  ::before {
      content: "";
      display: block;
      width: 22px;
      height: 22px;
      border-radius: 3px;
      border: 1px solid #ccc;
      position: absolute;
      left: -35px;
      cursor: pointer;
      top: 50%;
      -webkit-transform: translateY(-50%);
          -ms-transform: translateY(-50%);
              transform: translateY(-50%);
  }
  
  @media (max-width: 1199.98px) {
    margin: 0  0  0 20px;
    min-width: 60px;
    font-size: 0.8em;
    
    ::before {
      content: "";
      display: block;
      width: 16px;
      height: 16px;
      left: -20px;
    }
  }
`;

const AddInfCheckboxWrapComponent = styled.div`
    display: -webkit-box;
     display: -webkit-flex;
     display: -ms-flexbox;
     display: flex;
     -webkit-flex-wrap: wrap;
          -ms-flex-wrap: wrap;
              flex-wrap: wrap;
     -webkit-box-pack: start;
     -webkit-justify-content: flex-start;
          -ms-flex-pack: start;
             justify-content: flex-start;      
`;

const AddInfCheckboxComponent = styled.div`
    margin-bottom: 12px;
    
    @media (max-width: 1199.98px) {
        margin-bottom: 8px;
    }
`;

export default class CheckboxesBlock extends Component {
    renderField = ({ input, index, name, type, id, label}) => (
        <div>
            <AddInfCheckboxInput
                {...input} key={index} name={name} type={type} id={id}/>
            <AddInfCheckboxInputLabelComponent htmlFor={id}>
                {label}
            </AddInfCheckboxInputLabelComponent>
        </div>
    );

    renderMainField = ({
                           input,
                           name,
                           type,
                           id,
                           label
                       }) => (
        <div>
            <AddInfCheckboxInput
                {...input} name={name} type={type} id={id}/>
            <AddInfCheckboxInputLabelComponent htmlFor="PrivacyPolicy">
                <div>{label[0]} <Link to="#">{label[1]}</Link></div>
            </AddInfCheckboxInputLabelComponent>
        </div>
    );

    render() {
        const {checkboxesTitle, checkboxes, mainCheckbox} = this.props;
        const checkboxesField = checkboxes.map((field, index) => {
            return (
                <AddInfCheckboxComponent>
                    <Field
                        key={index}
                        name={field.name}
                        type={field.type}
                        value={field.value}
                        id={field.id}
                        label={field.label}
                        component={this.renderField}
                    />
                </AddInfCheckboxComponent>
            )
        });
        return (
            <div>
                <AddInfTitleComponent>
                    <AddInfTitleSpanComponent>
                        { checkboxesTitle }
                    </AddInfTitleSpanComponent>
                </AddInfTitleComponent>

                <AddInfCheckboxWrapComponent>
                    {checkboxesField}
                </AddInfCheckboxWrapComponent>

                <AddInfCheckboxMainComponent>
                    <Field
                        name={mainCheckbox.name}
                        type={mainCheckbox.type}
                        id={mainCheckbox.id}
                        label={mainCheckbox.label}
                        component={this.renderMainField}
                    />
                </AddInfCheckboxMainComponent>
            </div>
        )
    }
}